package com.app.tums.heednow;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayout;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.*;
import com.app.tums.heednow.list.AnswerObject;
import com.app.tums.heednow.list.FeedbackData;
import com.app.tums.heednow.list.QuestionList;
import com.app.tums.heednow.list.QuestionObject;
import com.app.tums.heednow.utils.AsyncRequest;
import com.app.tums.heednow.utils.DetectConnection;
import com.app.tums.heednow.utils.SessionManager;
import com.app.src.main.R;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class QuestionnaireActivity extends AppCompatActivity implements AsyncRequest.OnAsyncRequestComplete

{

    String storeDetails = "";
    QuestionList quesList;
    TextView txtquesCounter;
    ImageView prevBtn;
    ImageView nextBtn;
    ImageView doneBtn;
    RelativeLayout pageContentArea;
    RelativeLayout footerControls;
    List<FeedbackData> feedDataList = new ArrayList<>();
    LinearLayout custForm;
    SessionManager sm;
    DatePickerDialog dobDatePicker;
    DatePickerDialog doaDatePicker;
    TextView txtDOB;
    TextView txtDOA;
    String txtDOB_Val = "";
    String txtDOA_Val = "";
    ProgressBar pb;
    JSONObject postDataSubmitted;
    boolean feedbackFinished = false;
    int curPage = 1;


    @Override
    public void onBackPressed() {
        if (!feedbackFinished) {
            AlertDialog.Builder builder = new AlertDialog.Builder(QuestionnaireActivity.this);
            builder.setTitle("End Feedback");
            builder.setMessage("Do you want to close this feedback session?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            QuestionnaireActivity.super.onBackPressed();
                            finish();
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            // Create the AlertDialog object and return it
            builder.show();
        }


    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questionnaire);

        quesList = QuestionList.getInstance();

        sm = new SessionManager(getApplicationContext());
        storeDetails = sm.getStoreDetails().get(SessionManager.OUTLET_DETAILS);


        //quesTitle = (TextView) findViewById(R.id.quesTitle);
        txtquesCounter = (TextView) findViewById(R.id.txtquesCounter);
        pageContentArea = (RelativeLayout) findViewById(R.id.pageContentArea);
        footerControls = (RelativeLayout) findViewById(R.id.footerControls);
        //quesOptionArea = (RelativeLayout) findViewById(R.id.optionsArea);

        prevBtn = (ImageView) findViewById(R.id.round_left_button);
        nextBtn = (ImageView) findViewById(R.id.round_right_button);
        doneBtn = (ImageView) findViewById(R.id.done_button);


        try {
            ImageView banner = (ImageView) findViewById(R.id.banner);
            String imageName = "banner_";
            JSONObject storedJSON = new JSONObject(storeDetails);
            imageName += String.valueOf(storedJSON.getInt("id")) + ".png";
            banner.setImageBitmap(loadImageBitmap(getApplicationContext(), imageName));
        } catch (JSONException e) {
            e.printStackTrace();
        }


        for (int i = 0; i < quesList.getQuestions().size(); i++) {
            boolean isSingle = true;
            LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
            QuestionObject qObj = quesList.getQuestions().get(i);
            if (quesList.getQuestions().size() > (i + 1)) {
                QuestionObject qObjNext = quesList.getQuestions().get(i + 1);
                if (qObj.getPriority() == qObjNext.getPriority()) {
                    isSingle = false;
                }
            }
            LinearLayout page;
            if (isSingle) {
                page = (LinearLayout) inflater.inflate(R.layout.one_question_layout, pageContentArea, false);
                page.setTag(R.id.questionIndex, i);
                page.setTag(R.id.pageColumns, 1);
                renderQuestion(i, (TextView) page.findViewById(R.id.quesTitle), (RelativeLayout) page.findViewById(R.id.optionsArea));
            } else {
                page = (LinearLayout) inflater.inflate(R.layout.two_question_layout, pageContentArea, false);
                page.setTag(R.id.pageColumns, 2);
                LinearLayout ques1 = (LinearLayout) inflater.inflate(R.layout.two_question_layout_column, page, false);
                renderQuestion(i, (TextView) ques1.findViewById(R.id.quesTitle), (RelativeLayout) ques1.findViewById(R.id.optionsArea));
                ques1.setTag(R.id.questionIndex, i);
                page.addView(ques1);
                LinearLayout ques2 = (LinearLayout) inflater.inflate(R.layout.two_question_layout_column, page, false);
                ques2.setTag(R.id.questionIndex, i + 1);
                renderQuestion(i + 1, (TextView) ques2.findViewById(R.id.quesTitle), (RelativeLayout) ques2.findViewById(R.id.optionsArea));
                page.addView(ques2);
                i++;
            }
            page.setVisibility(View.GONE);

            pageContentArea.addView(page);
        }

        pageContentArea.getChildAt(0).setVisibility(View.VISIBLE);

        //CUSTOMER FORM
        LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
        custForm = (LinearLayout) inflater.inflate(R.layout.customer_info_form, pageContentArea, false);
        custForm.setVisibility(View.GONE);
        pageContentArea.addView(custForm);
        txtDOB = (TextView) custForm.findViewById(R.id.date_of_birth);
        txtDOA = (TextView) custForm.findViewById(R.id.date_of_anniversary);
        TextView quesTitle = (TextView) custForm.findViewById(R.id.quesTitle);
        quesTitle.setText("Tell us a little about yourself.");
        Calendar newCalendar = Calendar.getInstance();
        dobDatePicker = new DatePickerDialog(QuestionnaireActivity.this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                txtDOB_Val = onDateSelected(txtDOB, year, monthOfYear, dayOfMonth);

            }

        }, 0, newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH)) {
            @Override
            protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                int year = getContext().getResources()
                        .getIdentifier("android:id/year", null, null);
                if (year != 0) {
                    View yearPicker = findViewById(year);
                    if (yearPicker != null) {
                        yearPicker.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onDateChanged(DatePicker view, int year, int month, int dayOfMonth) {
                super.onDateChanged(view, year, month, dayOfMonth);
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, month, dayOfMonth);
                SimpleDateFormat dateFormatter = new SimpleDateFormat("dd MMM", Locale.US);
                dobDatePicker.setTitle(dateFormatter.format(newDate.getTime()));
            }
        };


        dobDatePicker.getDatePicker().setCalendarViewShown(false);
        txtDOB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dobDatePicker.show();
            }
        });


        doaDatePicker = new DatePickerDialog(QuestionnaireActivity.this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                txtDOA_Val = onDateSelected(txtDOA, year, monthOfYear, dayOfMonth);
            }

        }, 0, newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH)) {
            @Override
            protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                int year = getContext().getResources()
                        .getIdentifier("android:id/year", null, null);
                if (year != 0) {
                    View yearPicker = findViewById(year);
                    if (yearPicker != null) {
                        yearPicker.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onDateChanged(DatePicker view, int year, int month, int dayOfMonth) {
                super.onDateChanged(view, year, month, dayOfMonth);
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, month, dayOfMonth);
                SimpleDateFormat dateFormatter = new SimpleDateFormat("dd MMM", Locale.US);
                doaDatePicker.setTitle(dateFormatter.format(newDate.getTime()));
            }
        };

        doaDatePicker.getDatePicker().setCalendarViewShown(false);

        txtDOA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doaDatePicker.show();
            }
        });

        try {
            JSONObject storedJSON = new JSONObject(storeDetails);
            int mobLen = Integer.parseInt(storedJSON.getString("mobileNoLength"));
            EditText phoneNo = (EditText) custForm.findViewById(R.id.Mobile_Number);
            phoneNo.setFilters(new InputFilter[]{new InputFilter.LengthFilter(mobLen)});
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Button btnDone = (Button) custForm.findViewById(R.id.btnFinish);
        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                preSubmitFeedback();
            }
        });
        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                curPage++;
                drawPageContent();
            }
        });

        pb = (ProgressBar) findViewById(R.id.pbar);
        pb.getProgressDrawable().setColorFilter(
                Color.GREEN, android.graphics.PorterDuff.Mode.SRC_IN);

        prevBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  renderQuestion("backward");
                curPage--;
                drawPageContent();
            }
        });
        drawPageContent();
    }

    public Bitmap loadImageBitmap(Context context, String imageName) {
        Bitmap bitmap = null;
        FileInputStream fiStream;
        try {
            fiStream = context.openFileInput(imageName);
            bitmap = BitmapFactory.decodeStream(fiStream);
            fiStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    private void preSubmitFeedback() {

        EditText nameTxt = (EditText) custForm.findViewById(R.id.user_name);
        EditText phoneNo = (EditText) custForm.findViewById(R.id.Mobile_Number);
        EditText emailTxt = (EditText) custForm.findViewById(R.id.txtEmail);
        EditText locality = (EditText) custForm.findViewById(R.id.txtLocality);


        String name = nameTxt.getText().toString();
        String phone = phoneNo.getText().toString();
        String email = emailTxt.getText().toString();

        try {
            JSONObject storedJSON = new JSONObject(storeDetails);
            if (name.trim().isEmpty() || name.length() < 3) {
                nameTxt.setError("at least 3 characters");
                return;
            }
            int mobLen = Integer.parseInt(storedJSON.getString("mobileNoLength"));
            if (phone.trim().isEmpty() || phone.length() != mobLen) {
                phoneNo.setError(mobLen + " digit phone number");
                return;
            }
            if (!email.trim().isEmpty() && !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                emailTxt.setError("Invalid email address");
                return;
            }

            for (int i = 0; i < pageContentArea.getChildCount() - 1; i++) {
                LinearLayout page = (LinearLayout) pageContentArea.getChildAt(i);
                if ((int) page.getTag(R.id.pageColumns) == 1) {
                    int quesId = (int) page.getTag(R.id.questionIndex);
                    setFeedbackData(quesId, (RelativeLayout) page.findViewById(R.id.optionsArea));
                } else {
                    for (int j = 0; j < 2; j++) {
                        LinearLayout ques = (LinearLayout) page.getChildAt(j);
                        int quesId = (int) ques.getTag(R.id.questionIndex);
                        setFeedbackData(quesId, (RelativeLayout) ques.findViewById(R.id.optionsArea));
                    }
                }
            }

            JSONObject jsonObjectFields = new JSONObject();
            jsonObjectFields.put("name", name);
            jsonObjectFields.put("phoneNo", phone);
            jsonObjectFields.put("emailId", email);
            jsonObjectFields.put("dob", txtDOB_Val);
            jsonObjectFields.put("doa", txtDOA_Val);
            jsonObjectFields.put("locality", locality.getText().toString());


            JSONObject postObject = new JSONObject();
            postObject.put("outletId", storedJSON.getInt("id"));
            postObject.put("tableNo", getIntent().getStringExtra("tableNo"));
            postObject.put("customer", jsonObjectFields);
            postObject.put("token", sm.getStoreDetails().get(SessionManager.DEVICE_TOKEN));
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
            String formattedDate = df.format(cal.getTime());
            postObject.put("date", formattedDate);

            JSONArray postArray = new JSONArray();
            for (int i = 0; i < feedDataList.size(); i++) {
                FeedbackData feedbackData = feedDataList.get(i);
                List<AnswerObject> multiRatings = feedbackData.getMultiOptions();
                if (multiRatings != null && multiRatings.size() > 0) {
                    for (int j = 0; j < multiRatings.size(); j++) {
                        JSONObject feedDetailObjectMulti = new JSONObject();
                        AnswerObject feedbackDataMulti = multiRatings.get(j);
                        feedDetailObjectMulti.put("questionId", feedbackData.getQuestionId());
                        feedDetailObjectMulti.put("answerId", feedbackDataMulti.getAnswer_id());
                        feedDetailObjectMulti.put("answerText", feedbackData.getAnswerText());
                        feedDetailObjectMulti.put("rating", feedbackDataMulti.getRating());
                        postArray.put(feedDetailObjectMulti);
                    }
                } else {
                    JSONObject feedDetailObject = new JSONObject();
                    feedDetailObject.put("questionId", feedbackData.getQuestionId());
                    feedDetailObject.put("answerId", feedbackData.getAnswerId());
                    feedDetailObject.put("answerText", feedbackData.getAnswerText());
                    feedDetailObject.put("rating", feedbackData.getRating());
                    postArray.put(feedDetailObject);
                }
            }
            postObject.put("feedbacks", postArray);
            postDataSubmitted = postObject;
            String[] params = new String[2];
            params[0] = "feedback/create";
            params[1] = postObject.toString();
            if (DetectConnection.checkInternetConnection(QuestionnaireActivity.this)) {
                AsyncRequest postFeed = new AsyncRequest(QuestionnaireActivity.this, "POST", "feedback");
                postFeed.setPreloaderString("Saving Data...");
                postFeed.execute(params);
            } else {
                storeFeedbackOffline();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private String onDateSelected(TextView control, int year, int monthOfYear, int dayOfMonth) {
        Calendar newDate = Calendar.getInstance();
        newDate.set(year, monthOfYear, dayOfMonth);
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd MMM", Locale.US);
        control.setText(dateFormatter.format(newDate.getTime()));
        dateFormatter = new SimpleDateFormat("yyyy-MM-dd 00:00:00", Locale.US);
        return dateFormatter.format(newDate.getTime());
    }

    private FeedbackData getFeedbackData(int quesId) {
        for (int i = 0; i < feedDataList.size(); i++) {
            if (feedDataList.get(i).getQuestionId() == quesId) {
                return feedDataList.get(i);
            }
        }
        return null;
    }


    private void drawPageContent() {
        for (int i = 0; i < pageContentArea.getChildCount(); i++) {
            pageContentArea.getChildAt(i).setVisibility(View.GONE);
        }
        pageContentArea.getChildAt(curPage - 1).setVisibility(View.VISIBLE);
        if (curPage == 1) {
            prevBtn.setVisibility(View.INVISIBLE);
        } else {
            prevBtn.setVisibility(View.VISIBLE);
        }
        if (curPage == pageContentArea.getChildCount()) {
            nextBtn.setVisibility(View.INVISIBLE);
        } else {
            nextBtn.setVisibility(View.VISIBLE);
        }
        txtquesCounter.setText(curPage + " of " + pageContentArea.getChildCount());
        pb.setProgress(Math.round(getPercentage(curPage, pageContentArea.getChildCount())));
    }

    private void renderQuestion(int index, TextView quesTitle, RelativeLayout quesOptionArea) {
        QuestionObject qObj = quesList.getQuestions().get(index);
        quesTitle.setText(qObj.getQuestionDesc());


        quesTitle.setText(qObj.getQuestionDesc());
        quesOptionArea.removeAllViews();
        List<AnswerObject> options = qObj.getOptions();

        FeedbackData feedData = getFeedbackData(qObj.getId());

        LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
        switch (qObj.getQuestionType()) {
            case '1':
                GridLayout gridLayout = (GridLayout) inflater.inflate(R.layout.qtype_single_select, quesOptionArea, false);
                quesOptionArea.addView(gridLayout);
                if (options.size() > 5) {
                    gridLayout.setColumnCount(2);
                }
                for (int j = 0; j < options.size(); j++) {
                    LinearLayout ansItem = (LinearLayout) inflater.inflate(R.layout.single_select_option_item, gridLayout, false);
                    TextView txtAns = (TextView) ansItem.findViewById(R.id.itemText);
                    ImageView imgAns = (ImageView) ansItem.findViewById(R.id.itemImage);
                    imgAns.setTag(options.get(j).getAnswer_id());
                    txtAns.setText(options.get(j).getAnswerDesc());
                    gridLayout.addView(ansItem);
                    ansItem.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            GridLayout grid = (GridLayout) view.getParent();
                            for (int k = 0; k < grid.getChildCount(); k++) {
                                View subView = grid.getChildAt(k);
                                ImageView img = (ImageView) subView.findViewById(R.id.itemImage);
                                img.setImageResource(R.drawable.empty_circle);
                            }
                            ImageView img = (ImageView) view.findViewById(R.id.itemImage);
                            grid.setTag(img.getTag());
                            img.setImageResource(R.drawable.full_circle_checked);
                        }
                    });
                    if (feedData != null && feedData.getAnswerId() == options.get(j).getAnswer_id()) {
                        imgAns.setImageResource(R.drawable.full_circle_checked);
                    }
                }
                break;
            case '2':
                switch (qObj.getAnswerSymbol()) {
                    case 1:
                        GridLayout ratingSmileys = (GridLayout) inflater.inflate(R.layout.qtype_single_rating_smiley, quesOptionArea, false);
                        ratingSmileys.setTag(R.id.answerID, options.get(0).getAnswer_id());
                        ratingSmileys.setTag(R.id.rating, 0);
                        float realWeightageSmiley = options.get(0).getRating() / 5;
                        for (int i = 0; i < ratingSmileys.getChildCount(); i++) {
                            int rateNum = (i + 1);
                            ImageView smiley = (ImageView) ratingSmileys.getChildAt(i);
                            smiley.setTag(rateNum);
                            smiley.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    GridLayout grid = (GridLayout) view.getParent();
                                    ImageView selImg = (ImageView) view;
                                    for (int k = 0; k < grid.getChildCount(); k++) {
                                        ImageView imgView = (ImageView) grid.getChildAt(k);
                                        imgView.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.white));
                                        imgView.setBackgroundResource(0);
                                    }
                                    grid.setTag(R.id.rating, selImg.getTag());
                                    selImg.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.black));
                                    selImg.setBackgroundResource(R.drawable.ic_brightness_1_black_24dp);
                                }
                            });
                        }
                        quesOptionArea.addView(ratingSmileys);
                        break;
                    case 2:
                        RatingBar ratingStars = (RatingBar) inflater.inflate(R.layout.qtype_single_rating_stars, quesOptionArea, false);
                        ratingStars.setNumStars(options.get(0).getWeightage());
                        ratingStars.setTag(options.get(0).getAnswer_id());
                        //Drawable progress = ratingStars.getProgressDrawable();
                        //DrawableCompat.setTint(progress, ContextCompat.getColor(getApplicationContext(), R.color.white));
                        //ratingStars.setProgressDrawable(progress);
                        quesOptionArea.addView(ratingStars);
                        break;
                }
                break;
            case '3':
                LinearLayout multiRating = (LinearLayout) inflater.inflate(R.layout.qtype_multiple_rating, quesOptionArea, false);
                quesOptionArea.addView(multiRating);
                switch (qObj.getAnswerSymbol()) {
                    case 1:
                        LinearLayout multiRatingHolder = (LinearLayout) multiRating.findViewById(R.id.multiRatings);
                        for (int j = 0; j < options.size(); j++) {
                            LinearLayout ansItem = (LinearLayout) inflater.inflate(R.layout.multiple_rating_item_smiley, multiRatingHolder, false);
                            ansItem.setId(j);
                            TextView txtAns = (TextView) ansItem.findViewById(R.id.txtLabel);
                            txtAns.setText(options.get(j).getAnswerDesc());
                            GridLayout ratingSmileysMulti = (GridLayout) ansItem.findViewById(R.id.ratingSmileys);
                            ratingSmileysMulti.setTag(R.id.answerID, options.get(j).getAnswer_id());
                            ratingSmileysMulti.setTag(R.id.rating, 0);
                            for (int i = 0; i < ratingSmileysMulti.getChildCount(); i++) {
                                int rateNum = (i + 1);
                                ImageView smiley = (ImageView) ratingSmileysMulti.getChildAt(i);
                                smiley.setTag(rateNum);
                                smiley.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        GridLayout grid = (GridLayout) view.getParent();
                                        ImageView selImg = (ImageView) view;
                                        for (int k = 0; k < grid.getChildCount(); k++) {
                                            ImageView imgView = (ImageView) grid.getChildAt(k);
                                            imgView.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.white));
                                            imgView.setBackgroundResource(0);
                                        }
                                        grid.setTag(R.id.rating, selImg.getTag());
                                        selImg.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.black));
                                        selImg.setBackgroundResource(R.drawable.ic_brightness_1_black_24dp);
                                    }
                                });
                            }
                            multiRatingHolder.addView(ansItem);
                        }
                        break;
                    case 2:
                        for (int j = 0; j < options.size(); j++) {
                            LinearLayout ansItem = (LinearLayout) inflater.inflate(R.layout.multiple_rating_item_stars, multiRating, false);
                            ansItem.setId(j);
                            TextView txtAns = (TextView) ansItem.findViewById(R.id.txtLabel);
                            txtAns.setText(options.get(j).getAnswerDesc());
                            RatingBar ratingStarsMulti = (RatingBar) ansItem.findViewById(R.id.ratingStars);
                            ratingStarsMulti.setTag(options.get(j).getAnswer_id());
                            //Drawable progressMulti = ratingStarsMulti.getProgressDrawable();
                            //DrawableCompat.setTint(progressMulti, ContextCompat.getColor(getApplicationContext(), R.color.white));
                            // ratingStarsMulti.setProgressDrawable(progressMulti);
                            multiRating.addView(ansItem);
                        }
                        break;
                }
                break;
            case '4':
                EditText openText = (EditText) inflater.inflate(R.layout.qtype_open_text, quesOptionArea, false);
                openText.setTag(0);
                quesOptionArea.addView(openText);
                break;
            case '5':
                GridLayout gridLayoutMulti = (GridLayout) inflater.inflate(R.layout.qtype_multi_select, quesOptionArea, false);
                quesOptionArea.addView(gridLayoutMulti);
                if (options.size() > 5) {
                    gridLayoutMulti.setColumnCount(2);
                }
                for (int j = 0; j < options.size(); j++) {
                    LinearLayout ansItem = (LinearLayout) inflater.inflate(R.layout.multi_select_option_item, gridLayoutMulti, false);
                    ansItem.setId(j);
                    TextView txtAns = (TextView) ansItem.findViewById(R.id.itemText);
                    ImageView imgAns = (ImageView) ansItem.findViewById(R.id.itemImage);
                    imgAns.setTag(R.id.answerID, options.get(j).getAnswer_id());
                    imgAns.setTag(R.id.checked, false);
                    txtAns.setText(options.get(j).getAnswerDesc());
                    gridLayoutMulti.addView(ansItem);
                    ansItem.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ImageView img = (ImageView) view.findViewById(R.id.itemImage);
                            boolean isChecked = (boolean) img.getTag(R.id.checked);
                            if (isChecked) {
                                img.setImageResource(R.drawable.ic_check_box_outline_blank);
                            } else {
                                img.setImageResource(R.drawable.ic_check_box);
                            }
                            img.setTag(R.id.checked, !isChecked);
                        }
                    });
                }
                break;
            case '6':
                GridLayout gridLayoutBinary = (GridLayout) inflater.inflate(R.layout.qtype_binary_select, quesOptionArea, false);
                quesOptionArea.addView(gridLayoutBinary);
                int len = options.size();
                if (options.size() > 2)
                    len = 2;
                for (int j = 0; j < len; j++) {
                    LinearLayout ansItem = (LinearLayout) inflater.inflate(R.layout.binary_select_option_item, gridLayoutBinary, false);
                    TextView txtAns = (TextView) ansItem.findViewById(R.id.itemText);
                    final ImageView imgAns = (ImageView) ansItem.findViewById(R.id.itemImage);
                    imgAns.setTag(options.get(j).getAnswer_id());
                    txtAns.setText(options.get(j).getAnswerDesc());
                    if (j == 0) {
                        imgAns.setImageResource(R.drawable.ic_tick);
                    } else {
                        imgAns.setImageResource(R.drawable.ic_cancel);
                    }
                    gridLayoutBinary.addView(ansItem);
                    ansItem.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            GridLayout grid = (GridLayout) view.getParent();
                            for (int k = 0; k < grid.getChildCount(); k++) {
                                View subView = grid.getChildAt(k);
                                ImageView img = (ImageView) subView.findViewById(R.id.itemImage);
                                img.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.white));
                                img.setBackgroundResource(0);
                            }
                            ImageView img = (ImageView) view.findViewById(R.id.itemImage);
                            grid.setTag(img.getTag());
                            img.setColorFilter(ContextCompat.getColor(getApplicationContext(), android.R.color.transparent));
                            img.setBackgroundResource(R.drawable.ic_brightness_1_black_24dp);
                        }
                    });
                }
                break;
        }

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Quicksand-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());
    }


    public void setFeedbackData(int index, RelativeLayout quesOptionArea) {

        QuestionObject qObj = quesList.getQuestions().get(index);
        List<AnswerObject> options = qObj.getOptions();

        try {
            JSONObject respJSON = new JSONObject(storeDetails);
            FeedbackData feedData = getFeedbackData(qObj.getId());
            if (feedData == null) {
                feedData = new FeedbackData();
                feedDataList.add(feedData);
            }
            feedData.setQuestionId(qObj.getId());
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
            String formattedDate = df.format(cal.getTime());
            feedData.setDate(formattedDate);
            feedData.setOutletId(respJSON.getInt("id"));
            feedData.setTableNo(getIntent().getStringExtra("tableNo"));
            switch (qObj.getQuestionType()) {
                case '1':
                    GridLayout gridLayout = (GridLayout) quesOptionArea.findViewById(R.id.singleSelectGridOptions);
                    if (gridLayout.getTag() != null)
                        feedData.setAnswerId(Integer.parseInt(gridLayout.getTag().toString()));
                    break;
                case '2':
                    switch (qObj.getAnswerSymbol()) {
                        case 1:
                            GridLayout ratingSmileys = (GridLayout) quesOptionArea.findViewById(R.id.singleRatingSmileys);
                            float realWeightageSmiley = options.get(0).getRating() / 5;
                            feedData.setAnswerId(Integer.parseInt(ratingSmileys.getTag(R.id.answerID).toString()));
                            feedData.setRating(Math.round(Integer.parseInt(ratingSmileys.getTag(R.id.rating) + "") * realWeightageSmiley));
                            break;
                        case 2:
                            RatingBar ratingStars = (RatingBar) quesOptionArea.findViewById(R.id.ratingStars);
                            feedData.setAnswerId(Integer.parseInt(ratingStars.getTag().toString()));
                            float realWeightage = options.get(0).getRating() / (options.get(0).getWeightage() == 0 ? 5 : options.get(0).getWeightage());
                            feedData.setRating(Math.round(ratingStars.getRating() * realWeightage));
                            break;
                    }
                    break;
                case '3':
                    LinearLayout multiRating = (LinearLayout) quesOptionArea.findViewById(R.id.multiRatings);
                    List<AnswerObject> multiRatings = new ArrayList<>();
                    switch (qObj.getAnswerSymbol()) {
                        case 1:
                            for (int j = 0; j < options.size(); j++) {
                                LinearLayout ansItem = (LinearLayout) multiRating.findViewById(j);
                                GridLayout ratingSmileysMulti = (GridLayout) ansItem.findViewById(R.id.ratingSmileys);
                                AnswerObject ansObj = new AnswerObject();
                                ansObj.setAnswer_id(Integer.parseInt(ratingSmileysMulti.getTag(R.id.answerID).toString()));
                                float realWeightageMulti = options.get(j).getRating() / 5;
                                ansObj.setRating(Math.round(Integer.parseInt(ratingSmileysMulti.getTag(R.id.rating) + "") * realWeightageMulti));
                                multiRatings.add(ansObj);
                            }
                            break;
                        case 2:

                            for (int j = 0; j < options.size(); j++) {
                                LinearLayout ansItem = (LinearLayout) multiRating.findViewById(j);
                                RatingBar ratingStarsMulti = (RatingBar) ansItem.findViewById(R.id.ratingStars);
                                AnswerObject ansObj = new AnswerObject();
                                ansObj.setAnswer_id(Integer.parseInt(ratingStarsMulti.getTag().toString()));
                                float realWeightageMulti = options.get(j).getRating() / (options.get(j).getWeightage() == 0 ? 5 : options.get(j).getWeightage());
                                ansObj.setRating(Math.round(ratingStarsMulti.getRating() * realWeightageMulti));
                                multiRatings.add(ansObj);
                            }

                            break;
                    }
                    feedData.setMultiOptions(multiRatings);
                    break;
                case '4':
                    EditText openText = (EditText) quesOptionArea.findViewById(R.id.input_comment_box);
                    feedData.setAnswerId(Integer.parseInt(openText.getTag().toString()));
                    feedData.setAnswerText(openText.getText().toString());
                    break;
                case '5':
                    GridLayout gridLayoutMulti = (GridLayout) quesOptionArea.findViewById(R.id.multiSelectGridOptions);
                    List<AnswerObject> multiOptions = new ArrayList<>();
                    for (int j = 0; j < options.size(); j++) {
                        LinearLayout ansItem = (LinearLayout) gridLayoutMulti.findViewById(j);
                        ImageView ansImg = (ImageView) ansItem.findViewById(R.id.itemImage);
                        if ((boolean) ansImg.getTag(R.id.checked)) {
                            AnswerObject ansObj = new AnswerObject();
                            ansObj.setAnswer_id(Integer.parseInt(ansImg.getTag(R.id.answerID).toString()));
                            multiOptions.add(ansObj);
                        }
                    }
                    feedData.setMultiOptions(multiOptions);
                    break;
                case '6':
                    GridLayout gridLayoutBinary = (GridLayout) quesOptionArea.findViewById(R.id.binarySelectGridOptions);
                    if (gridLayoutBinary.getTag() != null)
                        feedData.setAnswerId(Integer.parseInt(gridLayoutBinary.getTag().toString()));
                    break;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public static float getPercentage(int n, int total) {
        float proportion = ((float) n) / ((float) total);
        return proportion * 100;
    }

    private void storeFeedbackOffline() {
        JSONArray unSyncArray;
        StringBuffer stringBuffer = new StringBuffer();
        String FILENAME = "offline_feed";
        FileOutputStream fos = null;
        try {
            //Attaching BufferedReader to the FileInputStream by the help of InputStreamReader
            BufferedReader inputReader = new BufferedReader(new InputStreamReader(
                    openFileInput(FILENAME)));
            String inputString;
            //Reading data line by line and storing it into the stringbuffer
            while ((inputString = inputReader.readLine()) != null) {
                stringBuffer.append(inputString + "\n");
            }
            try {
                unSyncArray = new JSONArray(stringBuffer.toString());
                unSyncArray.put(postDataSubmitted);
                fos = openFileOutput(FILENAME, Context.MODE_PRIVATE);
                fos.write(unSyncArray.toString().getBytes());
                fos.close();
                finishFeedback();
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void asyncResponse(String response, String label) {
        if (response != null && response.contains("SUCCESS")) {
            finishFeedback();
            return;
        }
        storeFeedbackOffline();
    }

    private void finishFeedback() {
        feedbackFinished = true;
        LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
        RelativeLayout thankYouPage = (RelativeLayout) inflater.inflate(R.layout.activity_thank_you, pageContentArea, false);
        pageContentArea.removeAllViews();
        pageContentArea.addView(thankYouPage);

        footerControls.setVisibility(View.INVISIBLE);
        ImageView homeBtn = (ImageView) thankYouPage.findViewById(R.id.gotohome);
        homeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}





