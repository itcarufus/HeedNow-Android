package com.app.tums.heednow.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import java.io.InputStream;
import java.net.URL;

/**
 * Created by Sandeep on 1/20/2017.
 */
public class DownloadImage extends AsyncTask<String, Void, Bitmap> {

    OnAsyncRequestComplete caller;
    Context context;
    ProgressDialog pDialog = null;
    String label;
    String _preloaderString = "Loading data...";

    public DownloadImage(Activity a){
        caller = (OnAsyncRequestComplete) a;
        context = a;
    }

    public interface OnAsyncRequestComplete {
        public void saveImage(Bitmap response);
    }
    private Bitmap downloadImageBitmap(String sUrl) {
        Bitmap bitmap = null;
        try {
            InputStream inputStream = new URL(sUrl).openStream();   // Download Image from URL
            bitmap = BitmapFactory.decodeStream(inputStream);       // Decode Bitmap
            inputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    @Override
    protected Bitmap doInBackground(String... params) {
        return downloadImageBitmap(params[0]);
    }
    protected void onPreExecute() {
        pDialog = new ProgressDialog(context);
        pDialog.setCancelable(false);
        pDialog.setMessage("Downloading..."); // typically you will define such
        pDialog.show();
    }
    protected void onPostExecute(Bitmap result) {
        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }
        caller.saveImage(result);
    }


}
