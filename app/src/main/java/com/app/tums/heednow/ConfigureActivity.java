package com.app.tums.heednow;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.app.tums.heednow.utils.AsyncRequest;
import com.app.tums.heednow.utils.DownloadImage;
import com.app.tums.heednow.utils.Installation;
import com.app.tums.heednow.utils.SessionManager;
import com.app.src.main.R;
import org.json.JSONException;
import org.json.JSONObject;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import java.io.FileOutputStream;
import java.io.IOException;


public class ConfigureActivity extends AppCompatActivity implements AsyncRequest.OnAsyncRequestComplete, DownloadImage.OnAsyncRequestComplete {
    EditText textConfigure;
    EditText textClient;
    EditText textVerify;
    TextView txtlabel;
    Button btnConfigure;
    Button btnVerify;
    TextView btnResend;
    private String address;
    SessionManager sessionManager;
    Intent tablePageIntent = null;
    ProgressDialog pDialog;
    String storeId = "";
    String clientId ="";

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configure);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Quicksand-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());
        btnConfigure = (Button) findViewById(R.id.btnConfigure);
        btnVerify = (Button) findViewById(R.id.btnVerify);
        btnResend = (TextView) findViewById(R.id.btnResend);
        textConfigure = (EditText) findViewById(R.id.TextConfigure);
        textVerify = (EditText) findViewById(R.id.TextOTP);
        txtlabel = (TextView) findViewById(R.id.txtlabel);
        textClient = (EditText) findViewById(R.id.Textclient);



        sessionManager = new SessionManager(getApplicationContext());
        btnConfigure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                {
                    storeId = textConfigure.getText().toString().trim();
                    clientId = textClient.getText().toString().trim();
                    tryRegister("Initializing...");
                   // Log.i("StoreId", storeId);
                }
            }
        });



        btnResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!storeId.isEmpty()){
                    tryRegister("Resending OTP...");
                }
            }
        });

        btnVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(!textVerify.getText().toString().isEmpty()){
                    try {
                        JSONObject jsonObjectFields = new JSONObject();
                        jsonObjectFields.put("storeId", storeId);
                        jsonObjectFields.put("clientId", clientId);
                        jsonObjectFields.put("installationId", Installation.id(getApplicationContext()));
                        jsonObjectFields.put("androidDeviceId", Installation.deviceId(getApplicationContext()));
                        jsonObjectFields.put("fingerprint", Build.FINGERPRINT);
                        jsonObjectFields.put("otp", textVerify.getText().toString());
                        String[] params = new String[2];
                        params[0] = "device/verify/";
                        params[1] = jsonObjectFields.toString();
                        AsyncRequest deviceInstall = new AsyncRequest(ConfigureActivity.this, "POST", "verifyDevice");
                        deviceInstall.setPreloaderString("");
                        if (pDialog == null) {
                            pDialog = new ProgressDialog(ConfigureActivity.this);
                            pDialog.setCancelable(false);
                            pDialog.setMessage("Verifying...");
                            pDialog.show();
                        }
                        deviceInstall.execute(params);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

    }
    private void tryRegister(String preloadStr){
        try {
            JSONObject registerReq = new JSONObject();
            registerReq.put("storeId", storeId);
            registerReq.put("clientId", clientId);
            registerReq.put("installationId", Installation.id(getApplicationContext()));
            registerReq.put("androidDeviceId", Installation.deviceId(getApplicationContext()));
            String[] params = new String[2];
            Log.i("postdata",registerReq.toString());
            params[0] = "device/register/";
            params[1] = registerReq.toString();
            AsyncRequest validateDevice = new AsyncRequest(ConfigureActivity.this, "POST", "register");
            validateDevice.setPreloaderString(preloadStr);
            validateDevice.execute(params);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void asyncResponse(String response, String label) {
        if (response != null && response.contains("SUCCESS")) {
            try {

                switch (label) {
                    case "register":
                        textConfigure.setVisibility(View.GONE);
                        textClient.setVisibility(View.GONE);
                        btnConfigure.setVisibility(View.GONE);
                        txtlabel.setText("One Time Password has been mailed");
                        textVerify.setVisibility(View.VISIBLE);
                        btnVerify.setVisibility(View.VISIBLE);
                        btnResend.setVisibility(View.VISIBLE);
                        break;
                    case "verifyDevice":
                        JSONObject validatedResp = new JSONObject(response);
                        sessionManager.registerDevice(validatedResp.getString("message"));
                        AsyncRequest getOutletInfo = new AsyncRequest(ConfigureActivity.this, "GET", "outlet");
                        getOutletInfo.setPreloaderString("");
                        pDialog.setMessage("Registering...");

                        getOutletInfo.execute("outlet/outletInfo/" + validatedResp.getString("message") + "/" + Installation.id(getApplicationContext()));
                        break;
                    case "outlet":
                        JSONObject storedJSON = new JSONObject(response);
                        sessionManager.configureStore(response);
                        sessionManager.configureQuestions(storedJSON.getString("questions"));
                        /*AsyncRequest get = new AsyncRequest(ConfigureActivity.this, "GET", "Questions");
                        get.setPreloaderString("");
                        get.execute("template/listQuestions/" + respJSON.getString("templateId"));*/
                        if (tablePageIntent == null) {


                            String FILENAME = "offline_feed";
                            FileOutputStream fos;
                            try {
                                String emptyFeedback = "[]";
                                fos = openFileOutput(FILENAME, Context.MODE_PRIVATE);
                                fos.write(emptyFeedback.getBytes());
                                fos.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            //JSONObject storedJSON = new JSONObject(sessionManager.getStoreDetails().get(SessionManager.OUTLET_DETAILS));
                            if (!storedJSON.getString("bannerUrl").equals("")) {
                                new DownloadImage(ConfigureActivity.this).execute(storedJSON.getString("bannerUrl"));
                            } else {
                                if (pDialog != null && pDialog.isShowing()) {
                                    pDialog.dismiss();
                                }
                                tablePageIntent = new Intent(getApplicationContext(), ChooseTableActivity.class);
                                startActivity(tablePageIntent);
                                finish();
                            }
                        }

                        break;
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            if(response == null){
                showError("Error occurred while retrieving data!");
            } else {
                switch (label) {
                    case "register":
                        showError("INVALID STORE ID");
                        break;

                    case "verifyDevice":
                        showError("Could not register, may be invalid OTP!");
                        break;
                }
            }
            if (pDialog != null && pDialog.isShowing()) {
                pDialog.dismiss();
            }
        }
    }

    private void showError(String err) {
        Toast tst = Toast.makeText(getApplicationContext(), err, Toast.LENGTH_LONG);
        tst.show();
    }

    @Override
    public void saveImage(Bitmap b) {
        FileOutputStream foStream;
        String imageName = "banner_";
        try {
            JSONObject storedJSON = new JSONObject(sessionManager.getStoreDetails().get(SessionManager.OUTLET_DETAILS));
            imageName += String.valueOf(storedJSON.getInt("id")) + ".png";
            try {
                foStream = openFileOutput(imageName, Context.MODE_PRIVATE);
                b.compress(Bitmap.CompressFormat.PNG, 100, foStream);
                foStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            tablePageIntent = new Intent(getApplicationContext(), ChooseTableActivity.class);
            startActivity(tablePageIntent);
            finish();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }
    }
}

