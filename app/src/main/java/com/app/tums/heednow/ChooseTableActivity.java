package com.app.tums.heednow;

/**
 * Created by Vijaya on 12-Dec-16.
 */

import android.app.ProgressDialog;
import android.content.*;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;
import com.app.tums.heednow.list.AnswerObject;
import com.app.tums.heednow.list.QuestionList;
import com.app.tums.heednow.list.QuestionObject;
import com.app.tums.heednow.list.TableList;
import com.app.tums.heednow.utils.*;
import com.app.src.main.R;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;


public class ChooseTableActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, AsyncRequest.OnAsyncRequestComplete, DownloadImage.OnAsyncRequestComplete {

    Spinner spin;
    SessionManager sessionManager;
    String tableNoSelected = "";
    TableList tableList;
    String outletDetails;
    Intent tablePageIntent = null;
    int syncFeedQueueCounter = 0;
    JSONArray unSyncArray = new JSONArray();
    ProgressDialog pDialog;
    BroadcastReceiver broadcastReceiver;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chose_table);
        sessionManager = new SessionManager(getApplicationContext());
        ;

        spin = (Spinner) findViewById(R.id.spinner);
        spin.setOnItemSelectedListener(this);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Quicksand-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());

        outletDetails = sessionManager.getStoreDetails().get(SessionManager.OUTLET_DETAILS);
        TextView txtStoreId = (TextView) findViewById(R.id.textstoreid);

        //*** SET TABLE LIST **/
        try {
            JSONObject jsonObjectOutlet = new JSONObject(outletDetails);
            String tableRange = jsonObjectOutlet.getString("tableNoRange");
            txtStoreId.setText(jsonObjectOutlet.getString("outletDesc"));
            String[] sep = tableRange.split(Pattern.quote(","));
            tableList = TableList.getInstance();
            tableList.clearList();
            for (int i = 0; i < sep.length; i++) {
                if (sep[i].trim().contains("-")) {
                    String[] range = sep[i].trim().split(Pattern.quote("-"));
                    for (int j = Integer.parseInt(range[0].trim()); j <= Integer.parseInt(range[1].trim()); j++) {
                        tableList.addTable(j + "");
                    }
                } else {
                    tableList.addTable(sep[i]);
                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        //*** SET QUESTION LIST **/
        String questionsData = sessionManager.getStoreDetails().get(SessionManager.QUESTION_LIST);
        try {
            JSONArray jsonArray = new JSONArray(questionsData);
            QuestionList questionList = QuestionList.getInstance();
            questionList.clearList();
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject quesJSONObj = jsonArray.getJSONObject(i);
                int parent_question_id = quesJSONObj.isNull("parentQuestionId") ? 0 : quesJSONObj.getInt("parentQuestionId");
                String parent_question_desc = quesJSONObj.isNull("parentQuestionDesc") ? "" : quesJSONObj.getString("parentQuestionDesc");
                int parent_answer_id = quesJSONObj.isNull("parentAnswerId") ? 0 : quesJSONObj.getInt("parentAnswerId");
                String parent_answer_desc = quesJSONObj.isNull("parentAnswerDesc") ? "" : quesJSONObj.getString("parentAnswerDesc");

                QuestionObject questionObject = new QuestionObject();
                questionObject.setId(quesJSONObj.getInt("id"));
                questionObject.setParentAnswerDesc(parent_answer_desc);
                questionObject.setParentQuestionDesc(parent_question_desc);
                questionObject.setQuestionDesc(quesJSONObj.getString("questionDesc"));
                questionObject.setQuestionType(quesJSONObj.getString("questionType").charAt(0));
                questionObject.setPriority(quesJSONObj.getInt("priority"));
                questionObject.setParentAnswerId(parent_answer_id);
                questionObject.setParentQuestionId(parent_question_id);
                questionObject.setAnswerSymbol(quesJSONObj.getInt("answerSymbol"));


                List<AnswerObject> option = new ArrayList<>();
                JSONArray jsonOptionsArray = quesJSONObj.getJSONArray("options");
                for (int j = 0; j < jsonOptionsArray.length(); j++) {
                    JSONObject ansJSONObj = jsonOptionsArray.getJSONObject(j);
                    AnswerObject answerObject = new AnswerObject();
                    answerObject.setAnswer_id(ansJSONObj.getInt("answer_id"));
                    answerObject.setAnswerDesc(ansJSONObj.isNull("answerDesc") ? "" : ansJSONObj.getString("answerDesc"));
                    answerObject.setRating(ansJSONObj.isNull("rating") ? 0 : ansJSONObj.getInt("rating"));
                    option.add(answerObject);
                }
                questionObject.setOptions(option);
                questionList.addQuestion(questionObject);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ArrayAdapter<String> aa = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, tableList.getTables());
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin.setAdapter(aa);


        Button btnConfigure = (Button) findViewById(R.id.btnProceed);
        btnConfigure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AsyncRequest get = new AsyncRequest(ChooseTableActivity.this, "GET", "version");
                get.setPreloaderString("Please Wait...");
                get.execute("settings/androidVersion");

            }
        });
        installListener();
    }

    private void setStatus(String status) {
        android.support.v7.app.ActionBar ab = getSupportActionBar();
        int offlineFeedLen = getOfflineFeeds().length();
        if (offlineFeedLen > 0) {
            status += " - Sync pending (" + offlineFeedLen + ")";
        }
        ab.setSubtitle(status);
    }

    public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long id) {
        tableNoSelected = tableList.getTables().get(position);
       /* Toast.makeText(getApplicationContext(), tableList.get(position), Toast.LENGTH_LONG).show();*/
    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {


    }

    private void installListener() {

        if (broadcastReceiver == null) {

            broadcastReceiver = new BroadcastReceiver() {

                @Override
                public void onReceive(Context context, Intent intent) {

                    Bundle extras = intent.getExtras();

                    NetworkInfo info = (NetworkInfo) extras
                            .getParcelable("networkInfo");

                    NetworkInfo.State state = info.getState();
                    if (state == NetworkInfo.State.CONNECTED) {
                        setStatus("ONLINE");
                    } else {
                        setStatus("OFFLINE");
                    }

                }
            };

            final IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
            registerReceiver(broadcastReceiver, intentFilter);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        android.support.v7.app.ActionBar ab = getSupportActionBar();
        String status = String.valueOf(ab.getSubtitle() != null ? ab.getSubtitle() : "");
        status = status.split(Pattern.quote(" -"))[0];
        setStatus(status);
    }

    @Override
    protected void onDestroy() {
        if (broadcastReceiver != null)
            unregisterReceiver(broadcastReceiver);
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_table_screen_settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.sync_setting:


                AsyncRequest get = new AsyncRequest(ChooseTableActivity.this, "GET", "outlet");
                get.setPreloaderString("Syncing..");
                get.execute("outlet/outletInfo/" + sessionManager.getStoreDetails().get(SessionManager.DEVICE_TOKEN) + "/" + Installation.id(getApplicationContext()));

                return true;
            case R.id.reset_setting:
                if (getOfflineFeeds().length() > 0) {
                    Toast tst = Toast.makeText(getApplicationContext(), "Reset not allowed! You have unsynced feedback.", Toast.LENGTH_LONG);
                    tst.show();
                    return false;
                }

                AlertDialog.Builder builder = new AlertDialog.Builder(ChooseTableActivity.this);
                builder.setTitle("Account Reset Confirmation!");
                builder.setMessage("The action is irreversible, all your offline data will get erased. Are you sure you want to reset the account?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                String FILENAME = "offline_feed";
                                try {
                                    String imageName = "banner_";
                                    JSONObject storedJSON = new JSONObject(sessionManager.getStoreDetails().get(SessionManager.OUTLET_DETAILS));
                                    imageName += String.valueOf(storedJSON.getInt("id")) + ".png";
                                    getApplicationContext().deleteFile(imageName);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                sessionManager.clearAccount();
                                getApplicationContext().deleteFile(FILENAME);
                                finish();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                // Create the AlertDialog object and return it
                builder.show();

                return true;
            case R.id.sync_feedback:
                if (DetectConnection.checkInternetConnection(ChooseTableActivity.this)) {
                    unSyncArray = getOfflineFeeds();
                    if (unSyncArray.length() > 0) {
                        pDialog = new ProgressDialog(ChooseTableActivity.this);
                        pDialog.setCancelable(false);
                        pDialog.show();
                        resolveFeedbackQueue();
                    } else {
                        Toast tst = Toast.makeText(getApplicationContext(), "No offline feedback to sync!", Toast.LENGTH_LONG);
                        tst.show();
                    }

                } else {
                    Toast tst = Toast.makeText(getApplicationContext(), "Cannot sync, you are offline!", Toast.LENGTH_LONG);
                    tst.show();
                }


                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private JSONArray getOfflineFeeds() {
        JSONArray jsonArray = new JSONArray();
        String FILENAME = "offline_feed";
        StringBuffer stringBuffer = new StringBuffer();
        FileOutputStream fos = null;
        try {
            //Attaching BufferedReader to the FileInputStream by the help of InputStreamReader
            BufferedReader inputReader = new BufferedReader(new InputStreamReader(
                    openFileInput(FILENAME)));
            String inputString;
            //Reading data line by line and storing it into the stringbuffer
            while ((inputString = inputReader.readLine()) != null) {
                stringBuffer.append(inputString + "\n");
            }
            try {
                return new JSONArray(stringBuffer.toString());

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new JSONArray();
    }

    private void resolveFeedbackQueue() {
        try {
            String[] params = new String[2];
            params[0] = "feedback/create";
            params[1] = unSyncArray.get(syncFeedQueueCounter).toString();

            AsyncRequest postFeed = new AsyncRequest(ChooseTableActivity.this, "POST", "SyncingFeedback");
            postFeed.setPreloaderString("");
            pDialog.setMessage("Syncing feedback " + (syncFeedQueueCounter + 1) + " of " + unSyncArray.length()); // typically you will define such
            postFeed.execute(params);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void asyncResponse(String response, String label) {
        if (response != null && response.contains("SUCCESS")) {
            try {

                switch (label) {
                    case "version":
                        PackageInfo pinfo = null;
                        try {
                            pinfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                        } catch (PackageManager.NameNotFoundException e) {
                            e.printStackTrace();
                        }
                        int versionNumber = pinfo.versionCode;
                        JSONObject versionJSON = new JSONObject(response);
                        if (versionJSON.getInt("versionCode") == versionNumber) {
                            proceedFeedback();
                        } else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(ChooseTableActivity.this);
                            builder.setTitle("New update available!");
                            builder.setMessage("To proceed, you must update the app from play store.")
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.dismiss();
                                        }
                                    });
                            builder.show();
                        }
                        break;
                    case "outlet":
                        JSONObject storedJSON = new JSONObject(response);
                        sessionManager.configureStore(response);
                        sessionManager.configureQuestions(storedJSON.getString("questions"));
                        /*AsyncRequest get = new AsyncRequest(ChooseTableActivity.this, "GET", "Questions");
                        get.setPreloaderString("Getting Data..");
                        get.execute("template/listQuestions/" + respJSON.getString("templateId"));*/


                            String imageName = "banner_";
                            imageName += String.valueOf(storedJSON.getInt("id")) + ".png";
                            deleteFile(imageName);
                            if (!storedJSON.getString("bannerUrl").equals("")) {
                                new DownloadImage(ChooseTableActivity.this).execute(storedJSON.getString("bannerUrl"));
                            } else {
                                reloadPage();
                            }


                        break;

                    case "SyncingFeedback":

                        String FILENAME = "offline_feed";
                        FileOutputStream fos;
                        try {
                            try {
                                JSONArray newList = new JSONArray();
                                for (int i = syncFeedQueueCounter + 1; i < unSyncArray.length(); i++) {
                                    newList.put(unSyncArray.get(i));
                                }
                                fos = openFileOutput(FILENAME, Context.MODE_PRIVATE);
                                fos.write(newList.toString().getBytes());
                                fos.close();

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        syncFeedQueueCounter++;
                        if (syncFeedQueueCounter < unSyncArray.length()) {
                            resolveFeedbackQueue();
                        } else {
                            if (pDialog != null && pDialog.isShowing()) {
                                pDialog.dismiss();
                            }
                            android.support.v7.app.ActionBar ab = getSupportActionBar();
                            String status = String.valueOf(ab.getSubtitle() != null ? ab.getSubtitle() : "");
                            status = status.split(Pattern.quote(" -"))[0];
                            setStatus(status);
                            Toast tst = Toast.makeText(getApplicationContext(), "All offline feedback are synced!", Toast.LENGTH_LONG);
                            tst.show();
                        }
                        break;
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            if (label.equals("version")) {
                proceedFeedback();
            }
            if (pDialog != null && pDialog.isShowing()) {
                pDialog.dismiss();
                Toast tst = Toast.makeText(getApplicationContext(), "Error occurred while syncing feedback!", Toast.LENGTH_LONG);
                tst.show();
            }
        }
    }

    private void proceedFeedback() {
        QuestionList questionList = QuestionList.getInstance();
        if (questionList.getQuestions().size() == 0) {
            Toast tst = Toast.makeText(getApplicationContext(), "No questions available! Try syncing!", Toast.LENGTH_LONG);
            tst.show();
            return;
        }
        if (!tableNoSelected.equals("")) {
            Intent intent = new Intent(getApplicationContext(), FeedbackStartupActivity.class);
            intent.putExtra("tableNoSel", tableNoSelected);
            startActivity(intent);
        }
    }

    @Override
    public void saveImage(Bitmap b) {
        FileOutputStream foStream;
        String imageName = "banner_";
        try {
            JSONObject storedJSON = new JSONObject(sessionManager.getStoreDetails().get(SessionManager.OUTLET_DETAILS));
            imageName += String.valueOf(storedJSON.getInt("id")) + ".png";
            try {
                foStream = openFileOutput(imageName, Context.MODE_PRIVATE);
                b.compress(Bitmap.CompressFormat.PNG, 100, foStream);
                foStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            reloadPage();

        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }
    }

    private void reloadPage() {
        startActivity(getIntent());
        Toast tst = Toast.makeText(getApplicationContext(), "Account synced successfully!", Toast.LENGTH_LONG);
        tst.show();
        finish();
    }
}


