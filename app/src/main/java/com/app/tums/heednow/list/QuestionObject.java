package com.app.tums.heednow.list;

import java.util.List;

/**
 * Created by System-3 on 12/30/2016.
 */
public class QuestionObject
{
    private int id;
    private String parentAnswerDesc;
    private String parentQuestionDesc;
    private String questionDesc;
    private char questionType;
    private int parentAnswerId;
    private int parentQuestionId;
    private int answerSymbol;
    private int priority;
    private List<AnswerObject> options;



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getParentAnswerDesc() {
        return parentAnswerDesc;
    }

    public void setParentAnswerDesc(String parentAnswerDesc) {
        this.parentAnswerDesc = parentAnswerDesc;
    }

    public String getParentQuestionDesc() {
        return parentQuestionDesc;
    }

    public void setParentQuestionDesc(String parentQuestionDesc) {
        this.parentQuestionDesc = parentQuestionDesc;
    }

    public String getQuestionDesc() {
        return questionDesc;
    }

    public void setQuestionDesc(String questionDesc) {
        this.questionDesc = questionDesc;
    }

    public char getQuestionType() {
        return questionType;
    }

    public void setQuestionType(char questionType) {
        this.questionType = questionType;
    }

    public int getParentAnswerId() {
        return parentAnswerId;
    }

    public void setParentAnswerId(int parentAnswerId) {
        this.parentAnswerId = parentAnswerId;
    }

    public int getParentQuestionId() {
        return parentQuestionId;
    }

    public void setParentQuestionId(int parentQuestionId) {
        this.parentQuestionId = parentQuestionId;
    }

    public int getAnswerSymbol() {
        return answerSymbol;
    }

    public void setAnswerSymbol(int answerSymbol) {
        this.answerSymbol = answerSymbol;
    }

    public List<AnswerObject> getOptions() {
        return options;
    }

    public void setOptions(List<AnswerObject> options) {
        this.options = options;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }
}
