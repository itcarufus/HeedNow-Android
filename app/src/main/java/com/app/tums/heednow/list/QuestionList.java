package com.app.tums.heednow.list;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sandeep on 12/29/2016.
 */
public class QuestionList {

    private List<QuestionObject> questions = new ArrayList<>();

    private static class SingletonHolder {
        private static final QuestionList INSTANCE = new QuestionList();
    }

    public static QuestionList getInstance() {
        return SingletonHolder.INSTANCE;
    }

    public List<QuestionObject> getQuestions() {
        return questions;
    }

    public void addQuestion(QuestionObject questionObject) {
        questions.add(questionObject);
    }

    public void clearList(){
        questions.clear();
    }
}
