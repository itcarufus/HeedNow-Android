package com.app.tums.heednow.list;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by System-3 on 12/30/2016.
 */
public class TableList {
    private List<String> tables = new ArrayList<>();

    private static class SingletonHolder {
        private static final TableList INSTANCE = new TableList();
    }

    public static TableList getInstance() {
        return TableList.SingletonHolder.INSTANCE;
    }

    public List<String> getTables() {
        return tables;
    }

    public void addTable(String tId) {
        tables.add(tId);
    }

    public void clearList(){
        tables.clear();
    }
}
