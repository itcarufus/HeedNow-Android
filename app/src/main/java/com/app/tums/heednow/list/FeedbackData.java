package com.app.tums.heednow.list;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sandeep on 1/2/2017.
 */
public class FeedbackData {

    private int outletId;
    private String date;
    private int questionId;
    private int answerId;
    private String answerText;
    private int rating;
    private String tableNo;
    private String billNo;
    private List<AnswerObject> multiOptions = new ArrayList<>();

    public int getOutletId() {
        return outletId;
    }

    public void setOutletId(int outletId) {
        this.outletId = outletId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    public int getAnswerId() {
        return answerId;
    }

    public void setAnswerId(int answerId) {
        this.answerId = answerId;
    }

    public String getAnswerText() {
        return answerText;
    }

    public void setAnswerText(String answerText) {
        this.answerText = answerText;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getTableNo() {
        return tableNo;
    }

    public void setTableNo(String tableNo) {
        this.tableNo = tableNo;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public List<AnswerObject> getMultiOptions() {

        return multiOptions;
    }

    public void setMultiOptions(List<AnswerObject> multiOptions) {
        this.multiOptions = multiOptions;
    }
}
