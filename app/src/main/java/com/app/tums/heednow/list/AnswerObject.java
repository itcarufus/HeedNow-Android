package com.app.tums.heednow.list;

/**
 * Created by System-3 on 12/27/2016.
 */
public class AnswerObject
{
    private String answerDesc;
    private int rating;
    private int weightage;
    private int answer_id;

    public String getAnswerDesc() {
        return answerDesc;
    }

    public void setAnswerDesc(String answerDesc) {
        this.answerDesc = answerDesc;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public int getAnswer_id() {
        return answer_id;
    }

    public void setAnswer_id(int answer_id) {
        this.answer_id = answer_id;
    }

    public int getWeightage() {
        return weightage;
    }

    public void setWeightage(int weightage) {
        this.weightage = weightage;
    }

    @Override
    public String toString() {
        return answerDesc;
    }
}
